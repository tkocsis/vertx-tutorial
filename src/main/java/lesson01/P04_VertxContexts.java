package lesson01;

import java.util.concurrent.atomic.AtomicInteger;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Vertx;

/**
 * Vertx thread context-ek: 
 * - minden Verticle kap egy dedikalt event-loop thread-et, amit a Context objektum tarol, ezt a getOrCreateContext()-tel lehet elerni
 * - egesz pontosan a verticle a start hivasanal kap egy context objektumot, ami tarolja, hogy melyik thread-en kell futtatni a verticle
 *   handlereit
 * - Mit jelent ez? ha egy futo kodhoz hozza van rendelve egy Context, akkor minden handler, amit ez a futo kod regisztral be, 
 *   megkapja ugyanazt a context objektumot, es azon keresztul ugyanazzal a thread-del lehet meghivni, amivel a start is hivodott 
 * - mashogy megfogalmazva: minden handler implementaciot az a thread fogja meghivni az eredmennyel, aki futas kozben beregisztralta valahova
 * - miert jo? a verticle mindaddig garantaltan thread safe marad, amig csak handlereken keresztul updatelejuk a mezoit
 * 
 * A contextet ugy kell elkepzelni, mint a futo kod futasi kornyezetet, benne van melyik thread-et kene hasznalni, milyen configot
 * 
 * A pelda utan erdemes az alabbi linkeken reszletesebben is olvasni az event loop - context kettosrol: 
 * - http://vertx.io/blog/an-introduction-to-the-vert-x-context-object/
 * - https://github.com/vietj/vertx-materials/blob/master/src/main/asciidoc/output/Demystifying_the_event_loop.adoc
 */
public class P04_VertxContexts {

	private static final AtomicInteger counter = new AtomicInteger(0);
	
	public static class FirstVerticle extends AbstractVerticle {

		int id = counter.getAndIncrement();
		
		public void start() throws Exception {
			/**
			 * Minden verticle deploynal az adott verticle-hoz letrjen a hozza tartozo context objektum
			 * Ketfele kepp erheto el:
			 */
			Context vertxContext = vertx.getOrCreateContext(); 
			if (vertxContext == context) {
				System.out.println("Context is available as member in verticle");
			}
			
			/**
			 * Hol jon kepbe ez a context es a thread? 
			 * 
			 * Alapesetben a verticle event-loop threadet kap, de a vertx csak korlatozott szamu event-loop threadet ad: 
			 * 	- default: processzor magok * 2 db
			 *  - vagy konfiguraciobol megadhato egyeb, de nem veletlen annyi, amennyi (multi reactor pattern)
			 * 
			 * Ha van 1000 verticle, es mindet az event loop threadek szolgaljak ki, akkor jon a szabaly: 
			 * AZ EVENT LOOP THREADEKET NEM TARTJUK FEL!
			 * 
			 * Nem kerulhet event loop thread altal vegrehajtott kodreszbe olyan fuggvenyhivas, ami sokaig szamol,
			 * vagy valamilyen eroforrasra var, vagy annak az eredmenyere. Roviden: event loop threadet nem blokkolunk.
			 * 
			 * Az, hogy a context objektumunk milyen thread-hez lett kotve, azt meg is tudja mondani: 
			 */
			System.out.println(id + " Started " + getThreadInfo() + " - Verticle started"
					+ " is event loop context: " + context.isEventLoopContext());
			
			/**
			 * Mit lehet tenni ha megis blocking kodot futtatunk? Worker thread poolokba tesszuk a kodot, pl. a vertx worker pooljaba,
			 * aki az eredmenyt raadasul a megfelelo thread-del fogja meghivni, mivel vertx-en belul a context objektum automatikusan 
			 * tovabb delegalodik:
			 */
			vertx.<String> executeBlocking(future -> {
				try {
					System.out.println(id + " Starting blocking code execution in: " + getThreadInfo());
					Thread.sleep(1000);
					future.complete("Finished blocking code process, started by " + id);
				} catch (Exception e) {
					future.fail(e);
				}
			}, res -> {
				/**
				 * Az eredmenyt a verticle startjanal latott thread fogja elvegezni.
				 */
				String result = res.result();
				System.out.println(id + " Blocking code execution result in " + getThreadInfo() + ": " + result);
			});
			/**
			 * Szorosan ide tartozik, hogy a verticle nem csak event-loop threaden keresztul futhat, hanem leteznek
			 * worker verticle-ok, amiket a worker pool threadjehez rendel hozza a vertx. Azok mar tartalmazhatnak blocking code
			 * futtatasat is, hiszen ugyanazon a threadeken futnak, mint az executeBlocking kodjai.
			 * 
			 * Hogy eri el, hogy a handler azon a thread-en fog futni, amelyikhez a verticle hozza van rendelve? 
			 * A context objektum runOnContext fuggvenyevel: 
			 */
			context.runOnContext(res -> {
				System.out.println("Running in verticle's context: " + getThreadInfo());
			});
			
			/**
			 * Alapbeallitasban ha tobb executeBlockingot adunk ki egymas utan, akkor azok egymas utan hajtodnak vegre.
			 * Pl. db muveletek igy konnyen serializalhatoak: insert-update-select
			 * 
			 * Kulon kapcsoloval lehet utasitani, hogy parhuzamosan hajtodjanak vegre
			 * 
			 * Ugyanez igaz a worker verticle-okre is, ugyan tartalmazhatnak blocking kodot, de a kodreszek vegrehajtasa
			 * verticle-n belul szekvencialisan torteneik, es a callbackek is a megfelelo context-en keresztul a megfelelo thread-del
			 * hivodnak. De letezik multithread worker verticle, amire ez nem igaz, es minden muvelete parhuzamosan zajlik, ilyen esetben
			 * viszont magunknak kell gondoskodni arrol, hogy a verticle internal state-je thread safe legyen.
			 */
			vertx.<String> executeBlocking(fut -> fut.complete("S1 " + Thread.currentThread()), res -> System.out.println(res.result()));
			vertx.<String> executeBlocking(fut -> fut.complete("S2 " + Thread.currentThread()), res -> System.out.println(res.result()));
			vertx.<String> executeBlocking(fut -> fut.complete("S3 " + Thread.currentThread()), res -> System.out.println(res.result()));
			
			vertx.<String> executeBlocking(fut -> fut.complete("P1 " + Thread.currentThread()), false, res -> System.out.println(res.result()));
			vertx.<String> executeBlocking(fut -> fut.complete("P2 " + Thread.currentThread()), false, res -> System.out.println(res.result()));
			vertx.<String> executeBlocking(fut -> fut.complete("P3 " + Thread.currentThread()), false, res -> System.out.println(res.result()));
			
			/**
			 * Minden handler tehat, amit ebbol a threadbol hozunk letre, ugyanezzel a threaddel lesz kiertekelve.
			 * Felteve persze, hogy az async library is igy gondolja: az osszes vertx library szerencsere igy gondolja.
			 */
			vertx.eventBus().<String> consumer("test-channel", message -> {
				System.out.println(id + " " + "Message arrived: " + getThreadInfo() + " " + message.body());
			});
			
			/**
			 * Ebbol kovetkezik, hogy nekunk is igy kell async libraryt irni, kulonben egyszercsak az addig thread safe verticle
			 * nem lesz tovabba thread safe. A kovetkezo reszben erre lesz egy pelda.
			 */
		}
	}

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Main thread: " + getThreadInfo() + " - main deploy");
		Vertx vertx = Vertx.vertx();
		
		/**
		 * Ketszer is deployoljuk a verticle-t, mindegyikhez kulon event loop thread lesz hozzarendelve
		 */
		vertx.deployVerticle(FirstVerticle.class.getName()); 
		vertx.deployVerticle(FirstVerticle.class.getName());  
		
		Thread.sleep(1000);
		vertx.eventBus().publish("test-channel", "Hi");
		Thread.sleep(1000);
		
		vertx.eventBus().close(res -> vertx.close());
	}

	private static String getThreadInfo() {
		return Thread.currentThread().getId() + " " + Thread.currentThread().getName();
	}
}
