package lesson01;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;

public class P01_VertxStandaloneApp {

	public static class FirstVerticle extends AbstractVerticle {
		
		public void start() throws Exception {
			System.out.println("Start verticle");
		}
		
		@Override
		public void stop() throws Exception {
			System.out.println("Stop verticle");
		}
		
	}
	
	public static void main(String[] args) throws InterruptedException {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(FirstVerticle.class.getName());
		
		Thread.sleep(1000);
		vertx.close(res -> {
			System.out.println("Closed");
		});
	}
	
}
