package lesson01;

import java.util.Arrays;

import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;

public class P03_VertxFutures {

	public static void main(String[] args) throws InterruptedException {
		// Basic usage
		Future<Integer> future = Future.future();
		future.setHandler(res -> {
			System.out.println(res.succeeded() + " " + res.failed() + " " + res.result());
		});
		future.complete(1);
		//future.fail(failureMessage);
		//future.fail(exception);
		
		// Completed future-kent letrehozva async library-nek visszateresi ertekere hasznlajuk, ld. kesobb
		future = Future.succeededFuture(2);
		future.setHandler(res -> {
			System.out.println(res.succeeded() + " " + res.failed() + " " + res.result());
		});
		
		future = Future.failedFuture(new IllegalAccessException("Failed message"));
		future.setHandler(res -> {
			System.out.println(res.succeeded() + " " + res.failed() + " " + res.result() + " " + res.cause());
		});
		
		future = Future.failedFuture("Failed message");
		future.setHandler(res -> {
			System.out.println(res.succeeded() + " " + res.failed() + " " + res.result() + " " + res.cause());
		});
		
		// Tipical usage a handler-re
		future = Future.future();
		future.setHandler(res -> {
			if (res != null && res.succeeded()) {
				int result = res.result();
				System.out.println("Success: " + result);
			} else {
				System.out.println("Fail: " + res != null ? res.cause() : " result is null");
			}
		});
		future.complete(3);
		
		/**
		 * A future-ok azert jok, es azert lehetnek hasznosak, mert azon kivul, hogy async  eredmenyt kapunk bennu, 
		 * lehet oket compose-alni. Vagyis tobb future eredmenyet osszevarni, illetve egymastol fuggo future-oket vegrehajtatni.
		 */
		
		Future<String> fut1 = Future.future();
		Future<String> fut2 = Future.future();
		Future<String> fut3 = Future.future();
		
		/**
		 * Return a composite future, succeeded when all futures are succeeded, failed when any future is failed.
		 * The returned future fails as soon as one of f1 or f2 fails.
		 */
		CompositeFuture.all(fut1, fut2, fut3).setHandler(res -> {
			if (res.succeeded()) {
				// ha mind sikeres, ide jutunk
			} else {
				// ha legalabb egy elhal, akkor ide
			}
		});
		
		/**
		 * Return a composite future, succeeded when any futures is succeeded, failed when all futures are failed.
		 * The returned future succeeds as soon as one of f1 or f2 succeeds.
		 */
		CompositeFuture.any(Arrays.asList(fut1, fut2, fut3)).setHandler(res -> {
			if (res.succeeded()) {
				// barmelyik sikeres, akkor ide jutunk, rogton amint az sikeres lesz
			} else {
				// ha mind elhal, akkor ide
			}
		});
		
		
		/**
		 * A future.compose-zal pedig chainelni lehet, vagyis amint az egyik elkeszult, hivodik a rakovetkezo, vegig a hatterben
		 */
		Future<Integer> complicatedCalculate = Future.future();
		complicatedCalculate.setHandler(res -> System.out.println("Calculate done: " + res.result()));
		
		Future<Integer> step1 = Future.future();
		step1.compose(res -> {
			// ha a fut1 sikeres, es megjelenik az eredmeny, akkor ide jutunk
			System.out.println("Step1 result: " + res);
			Future<Integer> step2 = Future.future();
			
			// a next az egy async eredmeny lehet, pl. egy db call, most helyben sikeresre allitjuk
			step2.complete(res - 1); // a bonyolult es hosszan tarto muvelet vegen egyet levonunk belole
			return step2;
		}).compose(res -> {
			System.out.println("Step2 result: " + res);
			Future<Integer> step3 = Future.future();
			
			// most megint szimulalunk egy async hivast, ami vegul kettovel megszorozza
			step3.complete(res * 2);
			return step3;
		}).compose(res -> {
			// a next eredmenyet megkaptuk, de csak akkor szamoltuk ki, ha a step1 sikeres volt
			System.out.println("Step3 result: " + res);
			
			// utolso lepesben meg hozzaddunk 10-et, de itt mar a kulso future-t completeljuk, 
			// hogy a chain-nek vege legyen, es megjelenjen benne az eredmeny
			complicatedCalculate.complete(res + 10);
		}, complicatedCalculate);
		
		// szimulaljuk, hogy a step1-ben async hivas hatasara megjelenik az eredmeny:
		step1.complete(10);
		
	}
}
