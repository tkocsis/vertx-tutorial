package lesson01;

import java.util.concurrent.CompletableFuture;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;

public class P02_AsyncInterfaces {

	/**
	 * Async vezerles alatt az alabbi interface-ek hasznalatat ertjuk, az alabbi modon:
	 * - a hivo fel tudomasul veszi, hogy a hivasra nem lesz azonnal valasza
	 * - a meghivott fuggveny azonnal visszater, es az eredemenyt a hatterben elkezdi szamolni
	 * - amint az eredmeny megvan, a hivo felet valamilyen modon ertesiti
	 */
	
	public interface Callback<T>  {
		void handleResult(T result);
	}
	
	public interface AsyncInterfaceBasic {
		
		public void getUsers(Callback<String> resultHandler);
		
		public CompletableFuture<String> getUsers();
		
	}
	
	/**
	 * Vertx-ben ezt a callback-et megvalosito osztalyt Handler-nek hivjak:
	 */
	public interface AsyncInterfaceBasic_Vertx {
		public void getUsers(io.vertx.core.Handler<String> result);
	}
	
	/**
	 * Pelda async implementacio java-s megvalositasban: a kereshez inditunk egy kulon szalat, 
	 * es az majd kikalkulaja, es ertesit, ha kesz
	 */
	public static class AsyncInterfaceImplementation implements AsyncInterfaceBasic_Vertx {
		
		@Override
		public void getUsers(Handler<String> result) {
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
						Thread.sleep(5000); // very long process
					} catch (InterruptedException e) {
						e.printStackTrace();
					} 
					result.handle("Username");
				}
			}).start();
		}
	}
	
	/**
	 * Pelda a hivo oldalra: a hivas utan azonnal visszater a vezerles a main thread-be, az eredmenyrol meg majd ertesulunk
	 * akkor, amikor a
	 */
	public static class AsyncCaller {
		public static void main(String[] args) {
			new AsyncInterfaceImplementation().getUsers(new Handler<String>() {
				
				@Override
				public void handle(String result) {
					System.out.println("Elso eredmeny: " + result);
				}
			});
			System.out.println("Visszakaptam a vezerlest");
			
			// ezt a tovabbiakban igy irjuk:
			new AsyncInterfaceImplementation().getUsers(result -> System.out.println("Masodik eredmeny: " + result));
			System.out.println("Megint itt tart a vezerlo szal");
			
			/**
			 * Hogy festene a kod szinkron hivassal? Mi lenne a kimenete?
			 */
		}
	}
	
	/**
	 * Problema: mi van akkor, ha a getUsers implementacioja hibara fut? 
	 * - latjuk hogy mar a Thread.sleep exception-jevel se tudunk mit kezdeni, nem hogy a felmerulo ezer masik hibaval
	 * - a java8 CompletableFuture kezeli a hibakat, de a Callback-ben nem tudunk sikertelen esettel visszajelezni
	 * - miert nem hasznlja a vertx? 1. regebbi mint a java8, 2. a CompletableFuture get() hivasa miatt
	 * - ha exceptoint nem tudunk dobni ugy, hogy azt valojaban a "handler dobja" 
	 * - persze bele lehet wrappelni, hogy a null string, a hiba, vagy Optional-t ad vissza
	 * 
	 * Megoldas: okositsuk fel a callback-et, igy lenyegeben eljutunk a Promise patternhez.
	 */
	
	public interface Callback_ErrorHandling<T> {
		void handleSuccess(T result);
		
		void handleError(Exception exception);
	}
	
	public interface AsyncInterface_ErrorHandling {
		public void getUsers(Callback_ErrorHandling<String> result);
	}
	
	public static class AsyncInterfaceImplementation_ErrorHandling implements AsyncInterface_ErrorHandling {
		
		@Override
		public void getUsers(Callback_ErrorHandling<String> result) {
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
						Thread.sleep(5000); // very long process
						result.handleSuccess("Username");
					} catch (Exception e) {
						// hiba eseten jelezzuk a callback-ben hogy hiba van
						result.handleError(e);
					} 
				}
			}).start();
		}
	}
	
	/**
	 * Ezzel a megoldassal az implementacioban hiba eseten a callback handleError fuggvenyet hivjuk a megfelelo hibakoddal, ellenkezo esetben
	 * pedig a handleSuccess-nek adjuk az eredmeny.
	 * 
	 * Vertx-ben ezt az AsyncResult interface valositja meg, az implementacioja pedig a vertx-es Future. 
	 * A vertx amugy is mindenhol interface-t hasznal, vagyis mindenhol, ahol interface-ben AsyncResult-ot latunk, 
	 * ott Future-t var, illetve Future-t fogunk kapni.
	 */
	
	public interface AsyncInterfaceVertx {
		// public void getUsers(Handler<Future<String>> handler);
		public void getUsers(Handler<AsyncResult<String>> handler);
		
		public Future<String> getUsers();
	}
	
	/**
	 * Pelda az implementaciora, es a hivasra:
	 */
	public static class AsyncImplementationVertx implements AsyncInterfaceVertx {

		@Override
		public void getUsers(Handler<AsyncResult<String>> handler) {
			// fuggveny prototipus: handler.handle(AsyncResult<String>);
			new Thread(() -> {
				try {
					Thread.sleep(4000);
					handler.handle(Future.succeededFuture("Username"));
				} catch (Exception e) {
					handler.handle(Future.failedFuture(e));
				}
			}).start();
		}

		@Override
		public Future<String> getUsers() {
			Future<String> future = Future.future();
			new Thread(() -> {
				try {
					Thread.sleep(4000);
					
					future.complete("username");
				} catch (Exception e) {
					future.fail(e);
				}
			}).start();
			return future;
		}
	}
	
	/**
	 * Es vegul a hasznalata:
	 */
	public static class AsyncImplementationVertxUsage {
		public static void main(String[] args) {
			System.out.println("Start async call with vertx classes");
			AsyncImplementationVertx asyncImpl = new AsyncImplementationVertx();
			asyncImpl.getUsers(handler -> {
				System.out.println("Eredmeny 1: " + handler.result());
			});
			System.out.println("Vezerlo szal tovahaladt a hivason");
			
			Future<String> future = asyncImpl.getUsers();
			future.setHandler(res -> {
				System.out.println("Eredmeny 2: " + res.result());
			});
			
			System.out.println("Vezerlo szal megint tovahaladt a hivason");
		}
	}
	
	/**
	 * A kovetkezo fejezetben a Future-okrol lesz szo bovebben
	 */
}
