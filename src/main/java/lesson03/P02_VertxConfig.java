package lesson03;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

/**
 * Kulonbozo helyekrol tud properties-eket behuzni
 * A fromat lehet:
 * - alapbol: json, propertes
 * - kulso library-bol meg tud: hocon, yaml
 * 
 * A type lehet 
 * - file: vertx.fileSystem().readFile(path, completionHandler);
 * - http: client.get(port, host, path
 * - directory: path es fileset megadasaval
 * - environment variables alapu
 * - eventbus alapu
 * - ...
 * 
 * Eloszor definialni kell, hogy honnan miket toltson be, aztan azokat sorban betolti.
 * Amennyiben adott kulcsot tobb helyrol is be tud rantani, akkor 
 * 
 * A problema, hogy letezo vertx instance kell hozza, vagyis tok jo lenne, de magara
 * a vertx konfiguraciojat nem tudjuk ebbol szedni, mert annak mar kesz kell lennie.
 *
 */
public class P02_VertxConfig {

	public static void main(String[] args) {

		Vertx vertx = Vertx.vertx();

		ConfigStoreOptions propertiesFile = new ConfigStoreOptions()
				.setType("file")
				.setFormat("properties")
				.setConfig(new JsonObject().put("path", "lesson03/config.properties"));

		ConfigStoreOptions jsonFile = new ConfigStoreOptions()
				.setType("file")
				.setFormat("json")
				.setConfig(new JsonObject().put("path", "lesson03/config.json"));
		
		ConfigRetriever retriever = ConfigRetriever.create(vertx, new ConfigRetrieverOptions()
					.addStore(propertiesFile)
					.addStore(jsonFile));

		retriever.getConfig(res -> {
			if (res.succeeded()) {
				System.out.println(res.result().encodePrettily());
			} else {
				System.out.println(res.cause());
			}
			vertx.close();
		});
	}
}
