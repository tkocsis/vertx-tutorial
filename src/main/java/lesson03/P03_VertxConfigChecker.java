package lesson03;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonObject;

/**
 * Van egy jo feature-e a vertx config toolnak, ami azt tudja, hogy periodikusan figyeli a config fajlokat, 
 * es ha valtozas van, akkor azt tudja jelezni. Igy adott esetben a config fajl (vagy eleresi uton levo adat) modositgatasaval
 * konfiguralni lehet a futo vertx alkalmazast.
 * 
 * Pl.:
 *  - a vertx appunk egy http endpointrol veszi a mukodesi konfigjat, pl. milyen eszkozoket pinglejen
 *  - egy erre a celra felepitett admin feluleten atallitjuk a pingelendo eszkozok listajat
 *  - az api endpoint innentol az uj listat adja vissza mar
 *  - a vertx app erzekeli a config modosulasat, es atall az uj config szerinti eszkozok pingelesere  
 *
 * Ezt a feature alapbol el, es 5 masodpercenkent csekkel. Amennyiben nem akarjuk, a setScanPeriod-ot -1 -re kell allitani.
 */
public class P03_VertxConfigChecker {
	
	public static void main(String[] args) throws Exception {

		// Classpath resource-kent olvassuk fel a fajlt, amit a vertx default cache-el, ezt false-ra kell allitani
		Vertx vertx = Vertx.vertx(new VertxOptions().setFileResolverCachingEnabled(false));

		ConfigStoreOptions jsonFile = new ConfigStoreOptions()
				.setType("file")
				.setFormat("json")
				.setConfig(new JsonObject().put("path", "lesson03/config-modify.json"));
		
		ConfigRetriever retriever = ConfigRetriever.create(vertx, new ConfigRetrieverOptions()
				.setScanPeriod(5000)
				.addStore(jsonFile));

		retriever.getConfig(res -> {
			if (res.succeeded()) {
				System.out.println("Initial config: " + res.result().encodePrettily());
			} else {
				System.out.println(res.cause());
			}
		});
		
		retriever.listen(change -> {
			System.out.println("Old configuration: " + change.getPreviousConfiguration().encodePrettily());
			System.out.println("New configuration: " + change.getNewConfiguration().encodePrettily());
		});
		
		System.out.println("Lets modify the config file (press ENTER to exit)");
		System.in.read();
		vertx.close();
	}
	
}
