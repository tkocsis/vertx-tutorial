package lesson03;

import java.util.concurrent.TimeUnit;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.EventBusOptions;
import io.vertx.core.http.ClientAuth;
import io.vertx.core.json.JsonObject;

/**
 * Mind a vertx, mind a verticle es alapvetoen minden vertx module valamilyen Option objektumot var, aminek a settereit
 * hivva tudjuk konfiguralni letrehozaskor az adott objektumot. 
 * 
 * Veticle-ok eseteben lehetoseg van egy setConfig()-gal egy JsonObject-et atadni, amit verticle-n belul a config() objektummal
 * erunk el.
 *
 */
public class P01_PassingConfiguration {

	public static class ConfigurableVerticle extends AbstractVerticle {
		
		@Override
		public void start() throws Exception {
			// JsonObject config = config();
			System.err.println("Config received: " + config());
			System.err.println("Config() is alias for context.config(): " + context.config());
			/**
			 * Ez utobbi azert hasznos info, mert ha egy async library-t irunk, aminek a vertx objektum at van adva,
			 * akkor a vertx.getOrCreateContext().config()-gal ugyanezeket el tudjuk erni.
			 */
			
			/**
			 * A config()-tol a JsonObject interface-szel kerunk el mindent:
			 */
			System.err.println(config().getString("config-key-1"));
			System.err.println(config().getInteger("config-key-2"));
			System.err.println(config().getJsonObject("config-key-3").getString("complex-config-key"));
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		Vertx vertx = Vertx.vertx(new VertxOptions()
				.setClustered(false)
				.setClusterHost("localhost")
				.setWorkerPoolSize(30)
				.setEventBusOptions(new EventBusOptions()
						.setClientAuth(ClientAuth.NONE))
				.setEventLoopPoolSize(16));
		
		vertx.deployVerticle(ConfigurableVerticle.class.getName(), new DeploymentOptions()
				.setWorker(false)
				.setInstances(1)
				.setMaxWorkerExecuteTime(TimeUnit.SECONDS.toNanos(10))
				.setConfig(new JsonObject()
					.put("config-key-1", "string config key")
					.put("config-key-2", 10)
					.put("config-key-3", new JsonObject().put("complex-config-key", "complex configuration"))));
		
		Thread.sleep(1000);
		vertx.close();
	}
}
