package lesson06;

import java.util.UUID;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.ext.web.Router;

/**
 * A websocket lehetove teszi, hogy ketiranyu, realtime kommunikaciot folytassunk a klienssel
 * Az alabbi peldaban a kovetkezot valositjuk meg:
 *  - a vertx-core-ban levo httpserver websocket handlerevel varjuk a bejovok websocket kapcsolatokat
 *  - csak a /logs-ra erkezo pathon valaszolunk websocket requestre
 *  - ami bejovok adat jon, azt kiirjuk, kozben 3 masodpercenkent kuldunk
 *  - a kuldesnel a kuldendo adatot egy dedikalt eventbus csatornarol szedjuk, tehat a program barmely 
 *    reszerol kuldott uzenetet el tudnank kuldeni a kliensnek
 *  - a kliens a P02_VertxWebsocketClient-ben talalhato, ami fogadja az adatokat, es masodpercenkent kuld o is valamit,
 *    demonstralva, hogy valoban ketiranyu kapcsolatrol beszelunk
 */
public class P02_VertxWebsocketServer extends AbstractVerticle {

	@Override
	public void start() {
		// egy sima route-ot felveszunk csak a teszteles miatt
		Router router = Router.router(vertx);
		router.get("/").handler(request -> {
			request.response().end("Hello");
		});
		
		// start webserver
		vertx.createHttpServer()
			.websocketHandler(websocket -> {
				System.out.println("New websocket connection");
				// websocket path only in logs
				if (!websocket.path().equals("/logs")) {
					websocket.reject();
				} else {
					// incoming data
					websocket.handler(buffer -> {
						System.out.println("Data received: " + buffer.toString());
					});
					
					// outgoing data
			    	MessageConsumer<String> consumer = vertx.eventBus().<String> consumer("websocket.send", msg -> {
			    		websocket.write(Buffer.buffer(msg.body()));
			    	});
			    	
			    	// ha zarul a websocket a kliens miatt, akkor a consumert is unregistraljuk az eventbusrol
			    	websocket.endHandler(endHandler -> {
			    		System.out.println("Websocket client left");
			    		consumer.unregister();
			    	});
				}
			})
			.requestHandler(router::accept)
	    	.listen(8000, "localhost", res -> {
	    		System.out.println("Server started and listening on http://localhost:8000");
	    		
	    		// start send random data to websocket.send eventbus channel
	    		vertx.setPeriodic(3000, timerId -> {
	    			vertx.eventBus().send("websocket.send", "This is a uuid: " + UUID.randomUUID());
	    		});
	    	});
	    
	}
	
	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(P02_VertxWebsocketServer.class.getName());
	}
}
