package lesson06;

import java.util.UUID;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;


/**
 * A vertx web egy nagyon komplex libary, minden van benne, ami egy webszerver-hez kellhet. Az alabbi peldaban a kovetkezoket valositjuk meg:
 *  - letrehozunk nehany route-ot es az oket kiszolgalgo fuggvenyeket implementaljuk
 *  - engedelyezzuk a CORS hivasokat
 *  - beallitunk egy static route handlert kozonseges fajlok (html, txt, jpeg, stb...) kiszolgalasara
 *
 */
public class P01_VertxWebServer extends AbstractVerticle {
	
	private String serverName = "vertx-webserver_" + UUID.randomUUID();
	
	@Override
	public void start() throws Exception {
		// letrehozzuk a route-okat
		Router router = Router.router(vertx);
	
		router.get("/").handler(requestHandler -> {
			requestHandler.response().end("Hello!");
		});
		
		router.get("/api/test").handler(requestHandler -> {
			requestHandler.response().end("Response from server: " + serverName);
		});
		
		// path parametert konnyen elerunk
		router.get("/api/test/:param").handler(requestHandler -> {
			String parameter = requestHandler.pathParam("param");
			requestHandler.response().end("Path param was: " + parameter);
		});
		
		// a router http method fuggvenyeire tehetunk endpointokat
		router.delete("/api/test").handler(requestHandler -> {
			requestHandler.response().end("Delete: " + serverName);
		});
		
		// alapbol a body-kat nem olvassa be beerkezo request eseten, de egy ilyennel megmondhatjuk, hogy 
		// a handlereinkat csak akkor hivja, ha a body-t beolvasta
		router.route().handler(BodyHandler.create());
		
		// ha van egy konyvtarunk, ami static fileokat tartalmaz, akkor ezzel tudjuk egyszeruen berakni oket
		router.route("/dashboard").handler(StaticHandler.create("lesson06/webroot"));
		
		// cors request-ek engedelyezese
	    router.route().handler(CorsHandler.create("*")
    	      .allowedMethod(HttpMethod.GET)
    	      .allowedMethod(HttpMethod.POST)
    	      .allowedMethod(HttpMethod.DELETE)
    	      .allowedMethod(HttpMethod.PUT)
    	      .allowedMethod(HttpMethod.OPTIONS)
    	      .allowedHeader("Content-Type")
    	      .allowedHeader("Authorization"));
	    
	    // most hogy osszeallitottunk minden route-ot, inditjuk a webservert
	    HttpServer httpServer = vertx.createHttpServer(new HttpServerOptions().setHost("0.0.0.0").setPort(8000));
	    httpServer
	    	.requestHandler(router::accept)
	    	.listen(res -> {
	    		System.out.println("Server started and listening on http://localhost:8000");
	    	});
	    
	}

	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(P01_VertxWebServer.class.getName());
	}
}
