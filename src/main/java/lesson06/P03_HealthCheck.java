package lesson06;

import java.util.Random;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.healthchecks.HealthCheckHandler;
import io.vertx.ext.healthchecks.Status;
import io.vertx.ext.web.Router;

/**
 * A HealthCheck egyszeru, altalunk megirt ellenorzo fuggvenyeket futtat es osszegez, az eredemenyt pedig
 * egy http endpointra bekotve tudja visszaadni.
 */
public class P03_HealthCheck extends AbstractVerticle {

	@Override
	public void start() throws Exception {
		Router router = Router.router(vertx);

		router.get("/").handler(requestHandler -> {
			requestHandler.response().end("Hello healthcheck!");
		});
		
		HealthCheckHandler healthCheckHandler = HealthCheckHandler.create(vertx);
		router.get("/health*").handler(healthCheckHandler);
		
		// definialhatunk kulonbozo key-ek alatt olyan fuggvenyeket, amik valamilyen erteket kernek le, vagy ellenoriznek
		// ettol az ertektol fugggoen a masodik parameter future-jet kell beallitani 
		healthCheckHandler.register("cpu-load", future -> {
			int cpuLoad = new Random().nextInt(100);
			if (cpuLoad < 80) {
				future.complete(Status.OK());
			} else {
				future.complete(Status.KO());
			}
		});
		
		// atadhatunk extra informaciot is
		healthCheckHandler.register("hdd-disk-status", future -> {
			JsonObject hddRaidStatus = getRaidStatus();
			if (isRaidOk()) {
				future.complete(Status.OK(hddRaidStatus));
			} else {
				future.complete(Status.KO(hddRaidStatus));
			}
		});
		
		// exceptiont is jelezhetjuk
		healthCheckHandler.register("exception-example", future -> {
			if (new Random().nextInt(10) % 2 == 0) {
				future.fail(new RuntimeException("Serious exception message"));
			} else {
				future.complete(Status.OK());
			}
		});
		
		vertx.createHttpServer(new HttpServerOptions().setHost("localhost").setPort(8000))
				.requestHandler(router::accept)
				.listen(res -> {
					System.out.println("Server started and listening on http://localhost:8000");
				});
	}

	@Override
	public void stop() throws Exception {
	}

	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(P03_HealthCheck.class.getName());
	}

	
	// mock functions for demonstrations
	private boolean isRaidOk() {
		return true;
	}
	private JsonObject getRaidStatus() {
		return new JsonObject()
				.put("hdd1", new JsonObject().put("health", "80%"))
				.put("hdd2", new JsonObject().put("health", "90%"));
	}
}
