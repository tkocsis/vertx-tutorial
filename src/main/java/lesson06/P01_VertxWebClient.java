package lesson06;

import io.vertx.core.Vertx;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.ext.web.codec.BodyCodec;

/**
 * A szerver legyen az elso lepes, es fusson, ha ezt futtatni szeretenenk!
 * 
 * A vertx-nek van tobb http kliens implementacioja is, a legmagasabb es emiatt legkonnyebben hasznalhato 
 * a webclient library kliense. 
 *
 */
public class P01_VertxWebClient {

	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		
		WebClient webClient = WebClient.create(vertx, new WebClientOptions().setDefaultHost("localhost").setDefaultPort(8000));
		webClient.get("/api/test") // itt mar csak a request uri-t adjuk meg
			.as(BodyCodec.string()) // tobbfele formaba tudja rogton konvertalni a kapott eredemnyt
			.send(handler -> {
				if (handler.succeeded()) {
					HttpResponse<String> result = handler.result();
					System.out.println("Status: " + result.statusCode() +" - " + result.statusMessage());
					System.out.println("Body: " + result.body());
				} else {
					System.out.println(handler.cause());
				}
				
				vertx.close(); // kilepunk most a teszt vegen
			});
	}		
}
