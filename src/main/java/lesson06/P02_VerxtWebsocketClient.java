package lesson06;

import java.util.concurrent.atomic.AtomicInteger;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;

/**
 * A vertx WebClient library nem tud websocket-et, csak a vertx-core-ban levo HttpClient.
 * Fusson a P02 websocket szerver, mielott ezt elinditjuk
 * A kliens fogadja az adatokat, es masodpercenkent kuld egy adatot is.
 */
public class P02_VerxtWebsocketClient {

	public static void main(String[] args) throws InterruptedException {
		Vertx vertx = Vertx.vertx();
		HttpClient client = vertx.createHttpClient(new HttpClientOptions().setDefaultHost("localhost").setDefaultPort(8000))
			.websocket("/logs", websocket -> {
				System.out.println("Websocket connection is active now");
				
				// bejovo adatok
				websocket.handler(buffer -> {
					System.out.println("Received data: " + buffer.toString());
				});
				
				// masodpercenkent kuldunk adatot a szerver fele
				AtomicInteger i = new AtomicInteger();
				long timerId = vertx.setPeriodic(1000, tick -> {
					String message = "Counter state: " + i.incrementAndGet();
					Buffer buffer = Buffer.buffer(message);
					websocket.write(buffer);
				});
				
				// kapcsolat vege eseten takaritas
				websocket.endHandler(endHandler -> {
					System.out.println("Websocket ended, stop timer");
					vertx.cancelTimer(timerId);
				});
			});
		
		Thread.sleep(20000);
		client.close();
		vertx.close();
	}
}
