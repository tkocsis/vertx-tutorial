package lesson02.dao;

import java.util.List;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;

public interface AsyncNvrDao {

	public static AsyncNvrDao create(Vertx vertx) {
//		return new AsyncNvrDaoImpl_VertxContext(vertx);
		return new AsyncNvrDaoImpl_SharedExecutor(vertx);
//		return new AsyncNvrDaoImpl_NonVertxThread(vertx);
	}
	
	public void getNVRs(Handler<AsyncResult<List<String>>> handler);
	
}
