package lesson02.dao;

import java.util.Arrays;
import java.util.List;

import io.vertx.core.AsyncResult;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;

/**
 * A vertx executeBlocking methodja valojaba vertx.getCreateOrContext().executeBlocking-ot hivja. 
 * Mi is a context-en keresztul erjuk el ebben a peldaban a worker poolt, es azon keresztul hajtjuk 
 * vegre a blocking code-ot.
 */
public class AsyncNvrDaoImpl_VertxContext implements AsyncNvrDao {
	
	Context context;
	
	protected AsyncNvrDaoImpl_VertxContext(Vertx vertx) {
		this.context = vertx.getOrCreateContext();
	}
	
	@Override
	public void getNVRs(Handler<AsyncResult<List<String>>> handler) {
		context.<List<String>> executeBlocking(fut -> {
			try {
				System.out.println("Starting calculation on " + Thread.currentThread());
				Thread.sleep(1500); // some heavy database access
				List<String> result = Arrays.asList("szeged #1", "szeged #2", "szeged #3");
				fut.complete(result);
			} catch (Exception e) {
				handler.handle(Future.failedFuture(e));
			}
		}, handler);
	}
}
