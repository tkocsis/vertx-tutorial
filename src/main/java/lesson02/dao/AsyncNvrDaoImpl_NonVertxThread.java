package lesson02.dao;

import java.util.Arrays;
import java.util.List;

import io.vertx.core.AsyncResult;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;

/**
 * Teljesen vertx mentesen kulso executorokkal, vagy a peldaban kulso thread-del is vegre lehet hajtani a szamolas igenyes feladatot.
 * Ebben az esetben a context.runOnContext fuggvenyebol intezzuk az eredmeny visszaadasat.
 */
public class AsyncNvrDaoImpl_NonVertxThread implements AsyncNvrDao {

	Context context;

	protected AsyncNvrDaoImpl_NonVertxThread(Vertx vertx) {
		/**
		 * Letaroljuk azt a context-et, amelyikbol az adott AsyncNvrDao objektum letrejott.
		 * A valaszadas ezen keresztul kell majd tortenjen.
		 */
		this.context = vertx.getOrCreateContext();
	}

	@Override
	public void getNVRs(Handler<AsyncResult<List<String>>> handler) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					System.out.println("Starting calculation on " + Thread.currentThread());
					Thread.sleep(1500); // some heavy database access
					List<String> result = Arrays.asList("szeged #1", "szeged #2", "szeged #3");
					context.runOnContext(res -> {
						System.out.println("Starting notification on " + Thread.currentThread());
						handler.handle(Future.succeededFuture(result));
					});

				} catch (Exception e) {
					context.runOnContext(res -> {
						handler.handle(Future.failedFuture(e));
					});
				}
			}
		}).start();
	}
}
