package lesson02.dao;

import java.util.Arrays;
import java.util.List;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.WorkerExecutor;

/**
 * Letre lehet hozni Shared Worker Executorokat, az azonos nevuek azonos executorra fognak mutatni.
 * Ez tarolja a letrehozaskor a context objektumot, es a result handlereket ezen keresztul fogja hivni.
 * 
 * Ebben a megoldasban tehat a vertx nagy kozos worker pooljat nem hasznaljuk, helyette a library-nak letrehozunk
 * egy 20 nagy thread poolt, aki csak ezen AsyncNvrDao peldanyok futasaert felel. Igy ha elrontjuk, akkor  csak a program
 * ezen részét akaszthatjuk meg.
 */
public class AsyncNvrDaoImpl_SharedExecutor implements AsyncNvrDao {

	WorkerExecutor executor;
	
	protected AsyncNvrDaoImpl_SharedExecutor(Vertx vertx) {
		executor = vertx.createSharedWorkerExecutor("nvr-dao-executor", 20);
	}
	
	@Override
	public void getNVRs(Handler<AsyncResult<List<String>>> handler) {
		getNvrsUsingVertxWorkerPool(handler);
	}

	private void getNvrsUsingVertxWorkerPool(Handler<AsyncResult<List<String>>> handler) {
		executor.<List<String>> executeBlocking(fut -> {
			try {
				System.out.println("Starting calculation on " + Thread.currentThread());
				Thread.sleep(1500); // some heavy database access
				List<String> result = Arrays.asList("szeged #1", "szeged #2", "szeged #3");
				fut.complete(result);
			} catch (Exception e) {
				handler.handle(Future.failedFuture(e));
			}
		}, handler);
	}
	

}
