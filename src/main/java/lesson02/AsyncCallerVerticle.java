package lesson02;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import lesson02.dao.AsyncNvrDao;

/**
 * Az alabbiakban egy async libraryt implementalunk ugy, hogy az a vertx-es thread management-be beleilleszkedjen, vagyis
 * a verticle futasa event-loopon van, a blocking code valamilyen elszeparalt threadeken, es az eredmenyt a verticle a hozza 
 * rendelt thread-en keresztul kapja meg.
 *  
 * Haromfele megvalositas veszunk vegig:
 *  - a blocking code a context objektum executeBlockingján fut a kozos worker pool-ban
 *  - a blocking code egy library-nek dedikalt shared executorban fut le
 *  - a blocking code-ot vertx fuggetlenul hajtuk vegre
 *  
 * Mindegyiknel a lenyeges dolog az, hogy a verticle-hoz rendelt event loop csupan inditja a folyamatot, 
 * es feldolgozza a kapott eredmenyt.
 */
public class AsyncCallerVerticle extends AbstractVerticle {

	private static final AtomicInteger counter = new AtomicInteger(0);
	
	int id;
	AsyncNvrDao nvrDao;
	
	@Override
	public void start() throws Exception {
		id = counter.getAndIncrement();
		System.out.println(id + " Starting thread on " + Thread.currentThread());
		
		// letrehozzuk egy peldanyt az async modon mukodo nvr dao objektumbol
		nvrDao = AsyncNvrDao.create(vertx);
		
		// Elkerjuk az nvr-eket, es varjuk az eredemenyt. A kerdes, hogy milyen threaden kapjuk meg.
		nvrDao.getNVRs(res -> {
			if (res != null && res.succeeded()) {
				List<String> nvrs = res.result();
				System.out.println(id +" Result arrived on " + Thread.currentThread() + ": " 
						+ nvrs.stream().collect(Collectors.joining(", ")));
			} else {
				// error handling
			}
		});
		
		System.out.println(id + " No more to do for now");
	}
	
	public static void main(String[] args) throws InterruptedException {
		Vertx vertx = Vertx.vertx();
		
		vertx.deployVerticle(AsyncCallerVerticle.class.getName());
		vertx.deployVerticle(AsyncCallerVerticle.class.getName());
		vertx.deployVerticle(AsyncCallerVerticle.class.getName());
		
		Thread.sleep(2000);
		vertx.close();
		/**
		 * Kerdesek: 
		 * - Valtozna-e valami, ha csak egy event loop threadunk van?
		 * - Mi tortenne, ha SharedExecutoros megvalositasban 1 hosszu a worker pool? (Vagy a kozos worker pool 1 hosszu?)
		 */
		
	}
}
