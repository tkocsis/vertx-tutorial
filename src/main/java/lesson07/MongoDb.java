package lesson07;

import java.util.List;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;

public class MongoDb extends AbstractVerticle {

	@Override
	public void start() throws Exception {

		Future<String> insertDone = Future.future();
		
		// az option-ben rengeteg mindent meg lehet adni:
		// http://vertx.io/docs/vertx-mongo-client/java/#_configuring_the_client
		MongoClient client = MongoClient.createShared(vertx, new JsonObject().put("db_name", "test-database"));
		client.dropCollection("test-collection", res -> {
			if (!res.succeeded()) {
				res.cause().printStackTrace();
				vertx.close();
				return;
			}
			System.out.println("Start insert some values");
			
			for (int i = 0; i < 1500; i++) {
				int current = i;
				
				JsonObject data = new JsonObject()
					.put("name", "some value " + i)
					.put("email", "some email")
					.put("counter", (i + 1))
					.put("address", new JsonObject()
						.put("city", "MyCity")
						.put("zip", "zipCode")
						.put("country", "MyCountry"))
					.put("favorites", new JsonArray()
						.add("Toyota")
						.add("Ford")
						.add("Aprillia"));
				
				client.insert("test-collection", data, insertResult -> {
					if (insertResult.succeeded()) {
						if (current == 1499) {
							insertDone.complete(insertResult.result());
						}
					} else {
						System.out.println("Failure: " + insertResult.cause());
					}
				});
			}
		});
		
		// ha az insert sikeres volt, akkor keszitunk egy lekerdezest
		insertDone.setHandler(res -> {
			// SELECT * FROM test-collection WHERE name = 'some value 12'
			// db.getCollection('test-collection').find({ name: "some value 12" });
			JsonObject query = new JsonObject().put("name", "some value 12");
			client.find("test-collection", query, this::queryHandler);
			
			// SELECT name, email, counter FROM test-collection ORDER BY counter ASC LIMIT 50,10 
			// db.getCollection('test-collection').find( { }, { name: 1, email: 1, counter: 1 }).sort({ counter: 1}).skip(50).limit(10)
			client.findWithOptions("test-collection", 
					new JsonObject(), 
					new FindOptions()
						.setFields(new JsonObject().put("name", 1).put("email", 1).put("counter", 1))
						.setLimit(10)
						.setSkip(50)
						.setSort(new JsonObject().put("counter", 1)),
					this::queryHandler);
			
			// SELECT * FROM test-collection WHERE counter IN (500, 600)
			// db.getCollection('test-collection').find({ counter: { $in: [500, 600] } }).limit(10)
			client.findWithOptions("test-collection",
					new JsonObject().put("counter", new JsonObject().put("$in", new JsonArray().add(500).add(600))),
					new FindOptions().setLimit(10),
					this::queryHandler);
			
			// SELECT * FROM test-collection WHERE counter < 30
			// db.getCollection('test-collection').find({ counter: { $lt: 30 } }).limit(10)
			client.findWithOptions("test-collection",
					new JsonObject().put("counter", new JsonObject().put("$lt", 30)),
					new FindOptions().setLimit(10),
					this::queryHandler);
			
			// SELECT * FROM test-collection WHERE counter > 10 AND counter <= 20
			// db.getCollection('test-collection').find({ $and: [ {counter: { $gt: 10 } }, {counter: { $lte: 20 } } ] })
			client.find("test-collection",
					new JsonObject()
						.put("$and", new JsonArray()
							.add(new JsonObject().put("counter", new JsonObject().put("$gt", 10)))
							.add(new JsonObject().put("counter", new JsonObject().put("$lte", 20)))
							
					), this::queryHandler);	
			
			
			
			// wait 2 sec then exit
			vertx.<Void> executeBlocking(fut -> {
				try {
					Thread.sleep(2000);
				} catch (Exception e) {
				}
				fut.complete();
			}, waitRes -> {
				client.close();
				vertx.close();
			});

			
		});
	}
	
	private void queryHandler(AsyncResult<List<JsonObject>> queryResult) {
		if (queryResult.succeeded()) {
			List<JsonObject> resultList = queryResult.result();
			resultList.stream()
					.forEach(item -> System.out.println(item.encodePrettily()));
		} else {
			System.out.println(queryResult.cause());
		}
	}
	
	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(MongoDb.class.getName());
	}
}
