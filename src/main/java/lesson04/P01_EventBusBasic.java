package lesson04;

import java.util.Objects;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

/**
 * Mit fogunk megvalositani:
 * - irunk egy verticle-t, ami matematikai muveleteket tud vegezni
 * - ebbol 10-et deployolunk, hogy legyen szamolasi tartalekunk boven
 * - worker verticle-kent deployoljuk, ami azt jelenti, hogy a handlerjei idoigenyesek lehetnek
 * - lesz nehany topicunk, ami a publish-subscribe és a request-response patterneket bemutatja
 */
public class P01_EventBusBasic {

	static AtomicInteger counter = new AtomicInteger();
	
	public static class CalculateVerticle extends AbstractVerticle {
		
		int id = counter.getAndIncrement();
		
		public void start() {
			/**
			 * A legegyszerubb consumer template: inline lambda message handler implementacioval
			 */
			vertx.eventBus().<String> consumer("system-messages", message -> {
				System.out.println(id + " " + message.body());
			});
			
			/**
			 * Javasolt legalabb kulon fuggvenyekbe szedni, a handler fuggvenyeket
			 * Ez lesz a send-receive fuggveny, a sok verticle kozul csak egy fogja vegrehajtani a feladatot,
			 * es valaszban elkuldi a kuldonek.
			 */
			vertx.eventBus().<JsonObject> consumer("calculate", this::onReceiveCalculate);
		}
		
		private void onReceiveCalculate(Message<JsonObject> message) {
			/**
			 * Az eventbus message handling/response folyamataban nincs hibakezeles. Ha varunk valaszt, 
			 * es a consumer oldalon exception lesz, arrol a hivo fel automatikusan nem ertesul! 
			 * Mindossze a timeout fog kiporogni, innen tudhatja a hivo fel, hogy baj van.
			 * 
			 * Ha egy handler elszall, akkor azert az exception kiirodik, de a message-re tehat automatikusa nem 
			 * megy semmi valasz, ezert erdemes biztositani, hogy valaszt varo hivas eseten mindig adjunk valaszt.
			 * 
			 * Na, ezert van az egesz fuggveny egy nagy try-catch blockban.
			 */
			try {
				JsonObject msg = message.body();
				
				Objects.requireNonNull(msg.getInteger("num1"), "'num1' not found");
				Objects.requireNonNull(msg.getInteger("num2"), "'num2' not found");
				Objects.requireNonNull(msg.getString("operation"), "'operation' not found");
				
				int num1 = msg.getInteger("num1");
				int num2 = msg.getInteger("num2");
				Integer result = null;
				switch (msg.getString("operation")) {
					case "sum":
						result = num1 + num2;
						break;
						
					case "multiply":
						result = num1 * num2;
						break;
						
					default:
						// nem tamogatott forma, visszaadjuk hogy ez nem megy
						throw new Exception("Operation not supported");
						
				}
				message.reply(new JsonObject().put("result", result).put("worker", id));
			} catch (Exception e) {
				message.reply(new JsonObject().put("error", e.getClass().getName() + ": " + e.getMessage()));
			}
		}
	}
	
	/**
	 * Az alabbiakban erdemes azt is megfigyelni, ahogy egy szekvencialis futas le lett forditva async programozasra.
	 * Elsore azonban koncentraljunk az eventbus mukodesere, es utana lehet megerteni a vezerlest.
	 * 
	 * Megjegyzes: a lent latott compose-os vezerles a demonstraciot szolgalja, a valos programozasban tobbnyire keves 
	 * ilyennel fogunk talalkozni, foleg hogy elterjedoben az RxJava-s felfogas vertx-en belul is.
	 */
	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		
		/**
		 * Async programozasnal kulonosen fontos, hogy a Future-oket ertelmesen nevezzuk el. Hivhatnank mindent
		 * fut1, fut2 stb-nek is, de talan egyel jobb, ha a folyamatot is tudjuk jelezni:
		 */
		Future<String> allDeployed = Future.future();
		
		/**
		 * Async verticle deployment: ha minden verticle elindult, az allDeployed future completed lesz 
		 */
		vertx.deployVerticle(CalculateVerticle.class.getName(), 
				new DeploymentOptions().setInstances(10).setWorker(true),
				allDeployed.completer());
		
		/**
		 * Lambdas kifejezesnel kulonosen fontos, hogy ha tudjuk, akkor ertelmesen jelezzuk hogy mi a handlerben-ben kapott
		 * String ertelme. Irhatnank oda str-t is, igy viszont eggyel jobb:
		 */
		allDeployed.compose(deploymentId -> {
			System.out.println("All verticle deployed, deployment ID is: " + deploymentId);
			vertx.eventBus().publish("system-messages", "System is online");
			return Future.succeededFuture();
		}).compose(sent -> {
			Future<Void> lastTaskDone = Future.future();
			
			for (int i = 0; i < 20; i++) {
				final boolean last = i == 19;
				JsonObject task = new JsonObject()
						.put("num1", new Random().nextInt(100))
						.put("num2", new Random().nextInt(100))
						.put("operation", new Random().nextInt(1) == 1 ? "sum" : "multiply");
				
				vertx.eventBus().<JsonObject> send("calculate", task, reply -> {
					if (reply.succeeded()) {
						Message<JsonObject> resultMessage = reply.result();
						JsonObject calculationResult = resultMessage.body();
						System.out.println("Task: " + task.encode() + "; result: " + calculationResult.encode());
					} else {
						// timed out
					}
					
					// ha az utolso task-ra is lett valami valaszunk, akkor a composition ezen resze veget ert
					if (last) {
						lastTaskDone.complete();
					}
				});
				
			}
			return lastTaskDone;
		}).compose(allTaskDone -> {
			/**
			 * Ha minden taskra jott valasz, akkor leallitjuk az eventbust, es ha az is leallt, akkor a vertx-et
			 */
			Future<Void> vertxClosed = Future.future();
			vertx.eventBus().close(res -> {
				vertx.close(vertxClosed.completer());
			});
			return vertxClosed;
		});
		
	}
	
	/**
	 * Feladat
	 * - Mi lenne a teendo, ha nem worker verticle-kent deployoljuk? Irjuk at a kodot ennek megfeleloen!
	 */
}
