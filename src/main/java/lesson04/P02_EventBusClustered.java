package lesson04;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

/**
 * Ha a vertx-et clusteringben inditjuk, akkor a Vertx.clusteredVertx-el inicializalodik a cluster manager. Ha vertx clustering van,
 * akkor az eventbus mindenkepp clustering eventbus lesz, mert a clusteredVertx static fv fixen beallitja true-ra. Tehat nincs olyan
 * opcio, hogy a vertx clusteringben fut, es az eventbus csak local. Ha clustering van, akkor figyelni kell, hogy a consumer mindig
 * cluster-wide regisztral, a localConsumer viszont csak local, viszont az ertesitesi szabaly a kovetkezo:
 * - Clustered modeban a message minden olyan node-nak elmegy, ahol van ra vonatkozo consumer, de csak azokhoz
 * - ha egy message megerkezik egy node-hoz, akkor valamennyi consumer es localConsumer is megkapja
 * 
 * Technikailag a consumer hivaskor egy shared map-be (AsyncMultiMap) es egy lokalis mapbe is beregisztral, ebbol elobbi cluster wide 
 * meg van osztva. es ez alapjan megy a remote ertesites. localConsumer hivas eseten a shared map-be nem kerul bele, csak a lokalisba.
 * A remote-bol beerkezo message pedig a local map-be regisztraltakat ertesiti ki (tehat mind a consumer, mind a localConsumer fv-vel
 * torteno feliratkozokat). 
 */
public class P02_EventBusClustered {
	
	static class Receiver {
		public static void main(String[] args) {
			Vertx.clusteredVertx(new VertxOptions(), res -> {
				Vertx vertx = res.result();
				
				vertx.eventBus().consumer("channel-1", message -> {
					System.out.println("Cluster consumer received message: " + message.body());
				});
				
				vertx.eventBus().localConsumer("channel-1", message -> {
					System.out.println("Local consumer received message: " + message.body());
				});
				
				System.out.println("Ready to receive");
			});
		}
	}

	static class Sender {
		public static void main(String[] args) {
			Vertx.clusteredVertx(new VertxOptions(), res -> {
				Vertx vertx = res.result();
				vertx.eventBus().publish("channel-1", "message body");
				System.out.println("Message sent");
			});
		}
	}
}
