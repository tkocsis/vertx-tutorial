package lesson04.p03;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import io.vertx.core.json.JsonObject;
import lesson04.p03.CalculationMessage.Operation;

public class CalculationMessageCodec implements MessageCodec<CalculationMessage, CalculationMessage> {

	@Override
	public CalculationMessage transform(CalculationMessage message) {
		// If a message is sent *locally* across the event bus.
		// This example sends message just as is
		return message;
	}

	@Override
	public String name() {
		// Each codec must have a unique name.
		// This is used to identify a codec when sending a message and for
		// unregistering codecs.
		return this.getClass().getName();
	}

	@Override
	public byte systemCodecID() {
		// Always -1
		return -1;
	}
	
	@Override
	public void encodeToWire(Buffer buffer, CalculationMessage message) {
		// Easiest ways is using JSON object
		JsonObject jsonToEncode = new JsonObject();
		jsonToEncode.put("num1", message.num1);
		jsonToEncode.put("num2", message.num2);
		jsonToEncode.put("operation", message.operation);

		// Encode object to string
		String jsonToStr = jsonToEncode.encode();

		// Length of JSON: is NOT characters count
		int length = jsonToStr.getBytes().length;

		// Write data into given buffer
		buffer.appendInt(length);
		buffer.appendString(jsonToStr);
	}

	@Override
	public CalculationMessage decodeFromWire(int position, Buffer buffer) {
		// My custom message starting from this *position* of buffer
		int _pos = position;

		// Length of JSON
		int length = buffer.getInt(_pos);

		// Get JSON string by it`s length
		// Jump 4 because getInt() == 4 bytes
		String jsonStr = buffer.getString(_pos += 4, _pos += length);
		JsonObject contentJson = new JsonObject(jsonStr);

		// Get fields
		int num1 = contentJson.getInteger("num1");
		int num2 = contentJson.getInteger("num2");
		Operation operation = Operation.valueOf(contentJson.getString("operation"));

		// We can finally create custom message object
		return new CalculationMessage(num1, num2, operation);
	}

}
