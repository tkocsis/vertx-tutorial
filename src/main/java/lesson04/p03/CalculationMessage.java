package lesson04.p03;

public class CalculationMessage {
	
	public enum Operation {
		SUM,
		MULTIPLY
	}

	final int num1;
	final int num2;
	final Operation operation;
	
	public CalculationMessage(int num1, int num2, Operation operation) {
		this.num1 = num1;
		this.num2 = num2;
		this.operation = operation;
	}
	
}
