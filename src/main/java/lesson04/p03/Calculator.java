package lesson04.p03;

import io.vertx.core.Future;
import io.vertx.core.Vertx;

public class Calculator {

	public static Future<Integer> calculate(Vertx vertx, CalculationMessage calculationMessage) {
		Future<Integer> result = Future.future();
		vertx.<Integer> executeBlocking(calcResult -> {
			switch (calculationMessage.operation) {
				case SUM:
					calcResult.complete(calculationMessage.num1 + calculationMessage.num2);
					break;
					
				case MULTIPLY:
					calcResult.complete(calculationMessage.num1 * calculationMessage.num2);
					break;
			}
		}, resultHandler -> {
			result.complete(resultHandler.result());
		});
		return result;
	}
}
