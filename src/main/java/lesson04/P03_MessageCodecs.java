package lesson04;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import lesson04.p03.CalculationMessage;
import lesson04.p03.CalculationMessage.Operation;
import lesson04.p03.CalculationMessageCodec;
import lesson04.p03.Calculator;

/**
 * Mit es hogyan lehet az eventbusra elkuldeni?
 * 
 * Primitiv tipusok wrappereit, stringet, JsonObject-et alapbol tud, de sajat objektumot csak akkor, ha megadunk hozza egy 
 * messagecodec implementaciot. Ebben ket dolgot kell implementalni: 
 *  - belso kezbesites eseten biztositani, hogy az uzenet immutable-kent menjen at (vagy copy, vagy immutable message-et hozunk letre)
 *  - clustered event bus eseten a serializacio-deserializaciot megirni Json formaba oda-vissza (polyglot miatt)
 */
public class P03_MessageCodecs {

	public static void main(String[] args) throws InterruptedException {
		Vertx vertx = Vertx.vertx();
		
		// primitiv tipusok wrapperei
		vertx.eventBus().<Integer> consumer("integer.channel", message -> {
			Integer body = message.body();
			System.out.println(body.intValue());
		});
		vertx.eventBus().publish("integer.channel", 4);
		
		// String message
		vertx.eventBus().<String> consumer("string.channel", message -> {
			System.out.println(message.body());
		});
		vertx.eventBus().publish("string.channel", "hello");
		
		// JsonObject
		vertx.eventBus().<JsonObject> consumer("json.channel", message -> {
			JsonObject body = message.body();
			System.out.println(body.encodePrettily());
		});
		vertx.eventBus().publish("json.channel", new JsonObject().put("key", "value"));
		
		// Sajat uzenettipus 
		vertx.eventBus().registerDefaultCodec(CalculationMessage.class, new CalculationMessageCodec());
		
		vertx.eventBus().<CalculationMessage> consumer("calculation.channel", message -> {
			CalculationMessage calculationMessage = message.body();
			Calculator.calculate(vertx, calculationMessage).setHandler(res -> {
				System.out.println("Calculation result: " + res.result());
			});
		});
		vertx.eventBus().publish("calculation.channel", new CalculationMessage(10, 10, Operation.SUM));
		vertx.eventBus().publish("calculation.channel", new CalculationMessage(5, 5, Operation.MULTIPLY));
		
		// vertx lezaras, kilepes
		Thread.sleep(1000);
		vertx.eventBus().close(res -> {
			vertx.close();
		});
	}
}
