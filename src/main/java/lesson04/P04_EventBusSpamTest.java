package lesson04;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

/**
 * Egy tesztprogram, amivel a context switcheket lehet tesztelni. Ha a deployment instanceot feljebb vesszuk, latni fogjuk,
 * hogy kevesebb uzenetet tudnak a verticle-ok egyenkent feldolgozni, de osszesen kozel ugyanannyit, mintha csak egy verticle van.
 */
public class P04_EventBusSpamTest {

	public static class Spammer extends AbstractVerticle {
		
		int processed = 0;
		
		@Override
		public void start() throws Exception {
			vertx.setPeriodic(1_000, res -> {
				System.out.println(Thread.currentThread().getName() + " Processed in the last second: " + processed);
				processed = 0;
			});
			
			vertx.eventBus().<JsonObject> consumer("spam-json", message -> {
				message.body(); // its a JsonObject
				processed++;
			});
			
			vertx.eventBus().<Integer> consumer("spam-integer", message -> {
				message.body(); // its a JsonObject
				processed++;
			});
			
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(Spammer.class.getName(), new DeploymentOptions().setInstances(1));
		
		vertx.executeBlocking(fut -> {
			while (true) {
				vertx.eventBus().publish("spam-json", new JsonObject().put("key", "value"));
//				vertx.eventBus().publish("spam-integer", 1);
			}
		}, res -> {});
	}
	
}
