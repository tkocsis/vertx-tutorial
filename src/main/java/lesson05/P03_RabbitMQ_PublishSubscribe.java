package lesson05;

import java.util.concurrent.atomic.AtomicInteger;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.rabbitmq.RabbitMQClient;

/**
 * 
 * A RabbitMQ messaging modeljeben az alabbiak jatszanak szerepet:
 * - producer
 * - consumer
 * - queue
 * - exchange
 * 
 * Az elso harom volt az elozo peldaban, most pedig bekerul a kepbe az exchange:
 * - http://www.rabbitmq.com/tutorials/tutorial-three-java.html
 * 
 * Roviden:
 * - a producer nem queue-nak kuld uzenetet, hanem mindig csak exchange-nek
 * - az exchange majd eldonti, hogy az uzenettel mi legyen, milyen queue-ba kell berakni, ha be kell egyaltalan
 * - a queue-bol pedig a consumer majd megkapja
 * - az exchange a type alapjan donti el, hogy mit csinaljon: direct, topic, headers, fanout
 * 
 * Ebbol a fanout-ot hasznaljuk. Az elozo pelda az alabbikepp modosul:
 * - nem queue-t, hanem exchange-et declaralunk, es abba publisholunk
 * - consumer oldalon minden RabbitMq csatlakozaskor letrehozunk egy uj, temporalis queue-t, így csak a legujabb uzeneteket kapjuk meg
 * - ez a temporalis queue ki is torlodik, ha lecsatlakozunk
 */
public class P03_RabbitMQ_PublishSubscribe {

	// Each parameter is optional
	static JsonObject config = new JsonObject()
			.put("user", "guest")
			.put("password", "guest")
			.put("host", "localhost")
			.put("port", 5672).put("virtualHost", "/")
			.put("connectionTimeout", 60000) // in milliseconds
			.put("requestedHeartbeat", 60) // in seconds
			.put("handshakeTimeout", 60000) // in milliseconds
			.put("requestedChannelMax", 5)
			.put("networkRecoveryInterval", 5000) // in milliseconds
			.put("automaticRecoveryEnabled", true);

	static String EXCHANGE_NAME = "broadcast_channel";

	public static class Sender {

		public static void main(String[] args) {
			Vertx vertx = Vertx.vertx();
			RabbitMQClient client = RabbitMQClient.create(vertx, config);
			client.start(connectResult -> {
				System.out.println("Client connected: " + connectResult.succeeded());
				AtomicInteger i = new AtomicInteger(0);

				client.exchangeDeclare(EXCHANGE_NAME, "fanout", false, false, declareResult -> {
					System.out.println("Exchange declared: " + declareResult.succeeded() + ", start sending");
					
					vertx.setPeriodic(1000, timerId -> {

						JsonObject message = new JsonObject()
								.put("properties", new JsonObject().put("contentType", "application/json"))
								.put("body", new JsonObject().put("counter", i.getAndIncrement()));
						
						client.basicPublish(EXCHANGE_NAME, "", message, sendingResult -> {
							if (sendingResult.cause() != null) {
								sendingResult.cause().printStackTrace();
							}
						});
					});
				});
			});
		}
	}

	public static class Receiver {

		public static void main(String[] args) {
			Vertx vertx = Vertx.vertx();
			RabbitMQClient client = RabbitMQClient.create(vertx, config);

			vertx.eventBus().consumer("eventbus.channel", msg -> {
				JsonObject json = (JsonObject) msg.body();
				System.out.println("Got message: " + json.getJsonObject("body").toString());
			});
				
			client.start(connectResult -> {
				System.out.println("Client connected: " + connectResult.succeeded());

				// temporalis queue letrehozasa
				client.queueDeclareAuto(queueDeclareResult -> {
					// ha letrejott, elkerjuk a nevet
					String tempQueueName = queueDeclareResult.result().getString("queue");
					
					// beallitjuk, hogy ez a queue kapcsolodjon az exchange-hez
					client.queueBind(tempQueueName, EXCHANGE_NAME, "", bindResult -> {
						client.basicConsume(tempQueueName, "eventbus.channel", consumeResult -> {
							if (consumeResult.succeeded()) {
								System.out.println("Queue consumer created");
							} else {
								consumeResult.cause().printStackTrace();
							}
						});
					});
				});
			});
		}
	}
}
