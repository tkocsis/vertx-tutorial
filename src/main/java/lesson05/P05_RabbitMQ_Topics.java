package lesson05;

import java.util.concurrent.atomic.AtomicInteger;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.rabbitmq.RabbitMQClient;

/**
 * Az elozo peldaban a fanout exchange type-ot hasznaltuk. A direct es a topic reszben fedi egymast, mindketto arra jo,
 * hogy az uzenetnek plusz osztalyozo parametert adjunk. Kuldo oldalrol ezt routing key-nek hivjuk, fogado oldalrol
 * pedig bindig key-nek:
 * - az exchange a routing key alapjan donti el, hogy a korabban a bind key alapjan hozza bekotott queue-k kozul kinek
 *   tovabbitsa az uzenetet
 */
public class P05_RabbitMQ_Topics {

	// Each parameter is optional
	static JsonObject config = new JsonObject()
			.put("user", "guest")
			.put("password", "guest")
			.put("host", "localhost")
			.put("port", 5672).put("virtualHost", "/")
			.put("connectionTimeout", 60000) // in milliseconds
			.put("requestedHeartbeat", 60) // in seconds
			.put("handshakeTimeout", 60000) // in milliseconds
			.put("requestedChannelMax", 5)
			.put("networkRecoveryInterval", 5000) // in milliseconds
			.put("automaticRecoveryEnabled", true);

	static String EXCHANGE_NAME = "numbers";

	public static class Sender {

		public static void main(String[] args) {
			Vertx vertx = Vertx.vertx();
			RabbitMQClient client = RabbitMQClient.create(vertx, config);
			AtomicInteger i = new AtomicInteger(0);
			
			// last step, all errors goes here
			Future<Void> sendStarted = Future.future();
			sendStarted.setHandler(res -> {
				if (res.succeeded()) {
					System.out.println("Sending started");
				} else {
					if (res.cause() != null) {
						res.cause().printStackTrace();
					}
				}
			});
			
			Future<Void> startResult = Future.future();
			client.start(startResult.completer());
			
			startResult.compose((v) -> {
				System.out.println("Client connected");
				
				Future<Void> declareResult = Future.future();
				client.exchangeDeclare(EXCHANGE_NAME, "topic", false, false, declareResult.completer());
				
				return declareResult;
			}).compose((v) -> {
				System.out.println("Exchange declared, start sending");
				
				vertx.setPeriodic(1000, timerId -> {

					int counter = i.getAndIncrement();
					JsonObject message = new JsonObject()
							.put("properties", new JsonObject().put("contentType", "application/json"))
							.put("body", new JsonObject().put("counter", counter));
					
					// ha 2-vel oszthato, akkor paros, kulonben paratlan
					String routingKey = counter % 2 == 0 ? "number.even" : "number.odd"; 
					
					client.basicPublish(EXCHANGE_NAME, routingKey, message, sendingResult -> {
						if (sendingResult.cause() != null) {
							sendingResult.cause().printStackTrace();
						}
					});
				});
			}, sendStarted);
		}
	}

	public static class Receiver {

		public static void main(String[] args) {
			Vertx vertx = Vertx.vertx();
			RabbitMQClient client = RabbitMQClient.create(vertx, config);
			
			// setting up eventbus channels
			vertx.eventBus().consumer("numbers.odd", msg -> {
				JsonObject json = (JsonObject) msg.body();
				System.out.println("Odd number received: " + json.getJsonObject("body").toString());
			});
			
			vertx.eventBus().consumer("numbers.even", msg -> {
				JsonObject json = (JsonObject) msg.body();
				System.out.println("Even number received: " + json.getJsonObject("body").toString());
			});
			
			vertx.eventBus().consumer("numbers", msg -> {
				JsonObject json = (JsonObject) msg.body();
				System.out.println("Number received: " + json.getJsonObject("body").toString());
			});
			
			// last step, all composed error goes here
			Future<Void> consumerCreated = Future.future(); 
			consumerCreated.setHandler(res -> {
				if (res.succeeded()) {
					System.out.println("Consumer created, receiving messages");
				} else {
					System.out.println("Error occured");
					if (res.cause() != null) {
						res.cause().printStackTrace();
					}
				}
			});
			
			Future<Void> connectResult = Future.future();
			client.start(connectResult.completer());
			
			connectResult.compose((v) -> {
				System.out.println("Client connected");
				
				// csak a number.odd routing key-el erkezo message-eket fogja megkapni
				client.queueDeclareAuto(queueDeclared -> {
					String tempQueueName = queueDeclared.result().getString("queue");
					client.queueBind(tempQueueName, EXCHANGE_NAME, "number.odd", bindResult -> {
						client.basicConsume(tempQueueName, "numbers.odd", res -> {
							System.out.println("Odd number queue set up: " + res.succeeded());
						});
					});
				});
				
				// csak a number.even routing key-el erkezo message-eket fogja megkapni
				client.queueDeclareAuto(queueDeclared -> {
					String tempQueueName = queueDeclared.result().getString("queue");
					client.queueBind(tempQueueName, EXCHANGE_NAME, "number.even", bindResult -> {
						client.basicConsume(tempQueueName, "numbers.even", res -> {
							System.out.println("Odd number queue set up: " + res.succeeded());
						});
					});
				});
				
				// number.* alapjan bindolo queue, minden szamot meg fog kapni, ami a numer.* topicra erkezik
				client.queueDeclareAuto(queueDeclared -> {
					String tempQueueName = queueDeclared.result().getString("queue");
					client.queueBind(tempQueueName, EXCHANGE_NAME, "number.*", bindResult -> {
						client.basicConsume(tempQueueName, "numbers", res -> {
							System.out.println("All number matching queue set up: " + res.succeeded());
						});
					});
				});
			}, consumerCreated);
		}
	}
}
