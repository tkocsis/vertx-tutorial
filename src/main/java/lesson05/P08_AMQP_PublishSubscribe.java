package lesson05;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import io.vertx.amqpbridge.AmqpBridge;
import io.vertx.amqpbridge.AmqpBridgeOptions;
import io.vertx.amqpbridge.AmqpConstants;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.eventbus.MessageProducer;
import io.vertx.core.json.JsonObject;

public class P08_AMQP_PublishSubscribe {
	
	static class Sender {
		public static void main(String[] args) {
			Vertx vertx = Vertx.vertx();
			AmqpBridge bridge = AmqpBridge.create(vertx);
			AtomicInteger count = new AtomicInteger();
			// Start the bridge, then use the event loop thread to process things
			// thereafter.
			bridge.start("localhost", 5672, "admin", "admin", res -> {
				if (res.failed()) {
					res.cause().printStackTrace();
					return;
				}
				// Set up a producer using the bridge, send a message with it.
				MessageProducer<JsonObject> producer = bridge.createProducer("topic://teszttopic");

				vertx.setPeriodic(1000, tid -> {
					JsonObject message = new JsonObject();
					message.put(AmqpConstants.BODY, count.incrementAndGet() + " vertx-message");
					System.out.println("Sending " + message);
					producer.send(message);
				});
			});
		}
	}

	static class Receiver {
		
		static UUID uuid = UUID.randomUUID();
		
		public static void main(String[] args) {
			Vertx vertx = Vertx.vertx();
			AmqpBridge bridge = AmqpBridge.create(vertx, new AmqpBridgeOptions()
					.setVirtualHost("mybroker"));
			// Start the bridge, then use the event loop thread to process things
			// thereafter.
			bridge.start("localhost", 5672, "admin", "admin", res -> {
				// Set up a consumer using the bridge, register a handler for it.
				MessageConsumer<JsonObject> consumer = bridge.createConsumer("topic://teszttopic");
				consumer.handler(vertxMsg -> {
					JsonObject amqpMsg = vertxMsg.body();
					String messageBody = amqpMsg.getString("body");

					System.out.println(uuid + " Received a message with body: " + messageBody);
				});
			});
		}
	}
}
