package lesson05;

import java.util.concurrent.atomic.AtomicInteger;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.rabbitmq.RabbitMQClient;

/**
 * Ugyanaz, mint a P03, csak kevesebb callback hell-t tartalmaz, a Future.compose-t hasznalja arra, hogy laposabb legyen a kod.
 * Ha jobban megnezzuk, akkor az elozo peldaban igencsak el lett sporolva a hibakezeles, a compose arra is jo, hogy az
 * egymastol fuggo folyamatok hibara futasat egy helyen kezeljuk.
 */
public class P04_RabbitMQ_PublishSubscribe_Flat {
	// Each parameter is optional
	static JsonObject config = new JsonObject()
			.put("user", "guest")
			.put("password", "guest")
			.put("host", "localhost")
			.put("port", 5672).put("virtualHost", "/")
			.put("connectionTimeout", 60000) // in milliseconds
			.put("requestedHeartbeat", 60) // in seconds
			.put("handshakeTimeout", 60000) // in milliseconds
			.put("requestedChannelMax", 5)
			.put("networkRecoveryInterval", 5000) // in milliseconds
			.put("automaticRecoveryEnabled", true);

	static String EXCHANGE_NAME = "broadcast_channel";

	public static class Sender {

		public static void main(String[] args) {
			Vertx vertx = Vertx.vertx();
			RabbitMQClient client = RabbitMQClient.create(vertx, config);
			AtomicInteger i = new AtomicInteger(0);
			
			// last step, all errors goes here
			Future<Void> sendStarted = Future.future();
			sendStarted.setHandler(res -> {
				if (res.succeeded()) {
					System.out.println("Sending started");
				} else {
					if (res.cause() != null) {
						res.cause().printStackTrace();
					}
				}
			});
			
			Future<Void> startResult = Future.future();
			client.start(startResult.completer());
			
			startResult.compose((v) -> {
				System.out.println("Client connected");
				
				Future<Void> declareResult = Future.future();
				client.exchangeDeclare(EXCHANGE_NAME, "fanout", false, false, declareResult.completer());
				
				return declareResult;
			}).compose((v) -> {
				System.out.println("Exchange declared, start sending");
				
				vertx.setPeriodic(1000, timerId -> {

					JsonObject message = new JsonObject()
							.put("properties", new JsonObject().put("contentType", "application/json"))
							.put("body", new JsonObject().put("counter", i.getAndIncrement()));
					
					client.basicPublish(EXCHANGE_NAME, "", message, sendingResult -> {
						if (sendingResult.cause() != null) {
							sendingResult.cause().printStackTrace();
						}
					});
				});
			}, sendStarted);
		}
	}

	public static class Receiver {

		public static void main(String[] args) {
			Vertx vertx = Vertx.vertx();
			RabbitMQClient client = RabbitMQClient.create(vertx, config);
			
			// setting up eventbus channel
			vertx.eventBus().consumer("eventbus.channel", msg -> {
				JsonObject json = (JsonObject) msg.body();
				System.out.println("Got message: " + json.getJsonObject("body").toString());
			});
			
			// last step, all composed error goes here
			Future<Void> consumerCreated = Future.future(); 
			consumerCreated.setHandler(res -> {
				if (res.succeeded()) {
					System.out.println("Consumer created, receiving messages");
				} else {
					System.out.println("Error occured");
					if (res.cause() != null) {
						res.cause().printStackTrace();
					}
				}
			});
			
			Future<Void> connectResult = Future.future();
			client.start(connectResult.completer());
			
			connectResult.compose((v) -> {
				System.out.println("Client connected");
				
				Future<JsonObject> queueDeclareFut = Future.future();
				client.queueDeclareAuto(queueDeclareFut.completer());
				
				return queueDeclareFut;
			}).compose(queueDeclareResult -> {
				String tempQueueName = queueDeclareResult.getString("queue");
				
				Future<String> queueName = Future.future();
				client.queueBind(tempQueueName, EXCHANGE_NAME, "", res -> {
					if (res.succeeded()) {
						queueName.complete(tempQueueName);
					} else {
						queueName.fail(res.cause());
					}
				});
				
				return queueName;
			}).compose((queueName) -> {
				client.basicConsume(queueName, "eventbus.channel", consumerCreated.completer());
			}, consumerCreated);
		}
	}
}
