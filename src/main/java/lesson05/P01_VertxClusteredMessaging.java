package lesson05;

import java.util.concurrent.atomic.AtomicInteger;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonObject;

/**
 * A vertx clustered messagingje nagyon jo, de csak addig, amig minden service fut, es nincs halozati problema. Amelyik uzenet
 * nem ert celba, az nem ert celba, az alkalmazasnak kell gondoskodnia rola, hogy odaerjen. Ebbol a szempontbol ugyanott van, mint
 * az akka, mindketten azt mondjak, oldja meg az alkalmazas azt, amire szuksege van.
 * 
 * Az alabbi peldat a kovetkezokepp erdemes futtatni: 
 * - Sender inditas
 * - Receiver inditas
 * - Receiver leallitas
 * - Receiver inditas
 * 
 * Megfigyelheto, hogy a leallitas idejen az uzenetek nem lettek kezbesitve. Kiprobalhato send, es publish kuldesi modokban is.
 */

public class P01_VertxClusteredMessaging {

	public static class Sender {
		public static void main(String[] args) {
			Vertx.clusteredVertx(new VertxOptions(), res -> {
				Vertx vertx = res.result();
				
				AtomicInteger i = new AtomicInteger(0);
				System.out.println("Start sending");
				vertx.setPeriodic(1000, timerId -> {
					vertx.eventBus().publish("channel", new JsonObject().put("counter", i.getAndIncrement()));
					//vertx.eventBus().send("channel", new JsonObject().put("counter", i.getAndIncrement()));
				});
			});
		}
	}
	
	public static class Receiver {
		public static void main(String[] args) {
			Vertx.clusteredVertx(new VertxOptions(), res -> {
				Vertx vertx = res.result();
				System.out.println("Start receiving");
				vertx.eventBus().<JsonObject> consumer("channel", message -> {
					System.out.println("Received: " + message.body().encode());
				});
			});
		}
	}
}
