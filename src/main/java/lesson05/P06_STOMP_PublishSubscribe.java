package lesson05;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.stomp.StompClient;
import io.vertx.ext.stomp.StompClientConnection;
import io.vertx.ext.stomp.StompClientOptions;

/**
 * Engedelyezni kell a RabbitMQ stomp plugint
 * sudo rabbitmq-plugins enable rabbitmq_stomp
 * sudo systemctl restart rabbitmq-server.service
 */
public class P06_STOMP_PublishSubscribe {

	public static void main(String[] args) {

		Vertx vertx = Vertx.vertx();
		StompClient.create(vertx, new StompClientOptions().setLogin("guest").setPasscode("guest").setVirtualHost("/"))
			.errorFrameHandler(frame -> {
				System.out.println(frame);
			}).connect(61613, "localhost", ar -> {
				if (ar.failed()) {
					ar.cause().printStackTrace();
					return;
				}
				StompClientConnection connection = ar.result();
				connection.connectionDroppedHandler(con -> {
					// The connection has been lost
					// You can reconnect or switch to another server.
				});

				vertx.setPeriodic(500, tid -> {
					connection.send("/queue/teszt", Buffer.buffer("Hello"), frame -> {
						System.out.println("Message processed by the server");
					});
				});

				connection.subscribe("/queue/teszt", frame -> {
					System.out.println("Just received a frame from /queue : " + frame);
				});
			});
	}
}
