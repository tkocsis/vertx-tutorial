## Vert.x tutorial

Lesson 01:
 * Hogy néz ki egy vertx standalone app
 * Mi az az async interface, és hogy lehet megvalósítani?
 * Vert.x Future használata
 * Vertx Thread kezelés, Context objektum
 
Lesson 02:
 * Teljes példa async interface implementációra
 
Lesson 03:
 * Configuration vert.x-ben
 * Config periodic scanner tudnivalók
 
Lesson 04:
 * EventBus basic: publish-subscribe, request-response patternek
 * Clustered eventbusról néhány szó
 * Saját objektumok az eventbuson: MessageCodec

 Lesson 05:
 * Messaging clustered eventbus-on keresztül
 * Messaging RabbitMQ-n keresztül: send, publish-subscribe, topics
 
 Lesson 06:
 * Vertx http szerver, és vertx webclient példa
 * Websocket szerver-kliensz
 * Vertx HealthCheck modul
 
 Lesson 07:
 * MongoDB client néhány insert és query függvénnyel bemutatva
 